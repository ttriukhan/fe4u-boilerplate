import { v4 as uuidv4 } from 'uuid';
import dayjs from 'dayjs';
import _ from 'lodash';


let users = []; //всі юзери

let filteredUsers = []; //відфільтровані юзери
let sortedUsers = []; //посортовані юзери
let foundUsers = []; //знайдені юзери
let favouriteUsers = []; //вподобані юзери

//кількість юзерів на запит, посилання
const numberOfUsers = 100;
const url = `https://randomuser.me/api/?results=${numberOfUsers}`;
const host = 'http://localhost:3000/posts';

let pages;

//функція, що записує до масиву користувачів і запускає малювання сторінки
async function getUsers() {
    const response = await httpGet(url);
    const dbResponse = await httpGet(host);
    const data = JSON.parse(response).results;
    const dbData = JSON.parse(dbResponse);
    users = [...data, ...dbData];
    createUsersArray();
    //users = users.concat(dbData);
    createTeachersGrid(users);
    createStatisticTable(users);
    createFavouritesLine();
    setInputs();
    console.log(users);
}

function httpGet(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.onload = function () {
        if (this.status === 200) {
          resolve(this.response);
        } else {
          const error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      };
      xhr.onerror = function () {
        reject(new Error('Network Error'));
      };
      xhr.send();
    });
}
  

//можливі курси
const possibleCourses = [
    "Mathematics",
    "Physics",
    "English",
    "Computer Science",
    "Dancing",
    "Chess",
    "Biology",
    "Chemistry",
    "Law",
    "Art",
    "Medicine",
    "Statistics",
];

//можливі кольори
const possibleColors = [
    "#FF0000",   // Red
    "#008000",   // Green
    "#0000FF",   // Blue
    "#FFFF00",   // Yellow
    "#FFA500",   // Orange
    "#800080",   // Purple
    "#FFC0CB",   // Pink
    "#A52A2A",   // Brown
    "#808080",   // Gray
    "#FF8080",   // Light Red
    "#80FF80",   // Light Green
    "#8080FF",   // Light Blue
    "#FFFF80",   // Light Yellow
    "#FFD580",   // Light Orange
    "#C080C0",   // Light Purple
    "#FFC0D0",   // Light Pink
    "#D55A5A",   // Light Brown
    "#A0A0A0",   // Light Gray
    "#FFFFFF",   // White
    "#FF1493",   // Deep Pink
    "#00FF7F",   // Spring Green
    "#1E90FF",   // Dodger Blue
    "#FFD700",   // Gold
    "#9ACD32",   // Yellow Green
    "#9370DB",   // Medium Purple
    "#D2691E",   // Chocolate
    "#20B2AA",   // Light Sea Green
    "#FF4500",   // Orange Red
    "#48D1CC"    // Medium Turquoise
];

//можливі нотатки
const possibleNotes = [
    "This is 24% a great day!",
    "Don't forget your umbrella.",
    "Learn something new today.",
    "Smile and be happy.",
    "Stay positive and keep going.",
    "Life is beautiful.",
    "Make a difference in the world.",
    "Believe in yourself.",
    "Chase your dreams.",
    "Be kind to others.",
];

//повертає рандомне значення з масиву
function getRandom(array) {
    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
}

//записати всих юзерів до всих масивів
function reset() {
    filteredUsers = [...users];
    foundUsers = [...users];
    sortedUsers = [...users];
}

//відформатувати рандомного користувача
function formatUser(user) {
    const formattedUser = {
    gender: user.gender? user.gender : "male",
    title: user.name? (user.name.title || "Mr") : "Mr",    
    full_name: `${user.name? (user.name.first || "John") : "John"} ${user.name? (user.name.last || "Weak") : "Weak"}`,
    city: user.location? (user.location.city || "Lubny") : "Lubny",
    state: user.location? (user.location.state || "Poltava") : "Poltava",
    country: user.location? (user.location.country || "Ukraine") : "Ukraine",
    postcode: user.location? (user.location.postcode || 35007) : 35007,
    coordinates: user.location? (user.location.coordinates || { latitude: "-42.1817", longitude: "-152.1685" }) : { latitude: "-42.1817", longitude: "-152.1685" },
    timezone: user.location? (user.location.timezone || { offset: "+9:30", description: "Adelaide, Darwin" }) : { offset: "+9:30", description: "Adelaide, Darwin" },
    email: user.email? (user.email || "vv@gmail.com") : "vv@gmail.com",
    b_date: user.dob? (user.dob.date || "1956-12-23T19:09:19.602Z") : "1956-12-23T19:09:19.602Z",
    age: user.dob? (user.dob.date? calculateAge(user.dob.date) : calculateAge("1956-12-23T19:09:19.602Z")) : calculateAge("1956-12-23T19:09:19.602Z"),
    phone: "+" + (user.phone? user.phone.replace(/\D/g, '') : "380702020820"),
    picture_large: user.picture? (user.picture.large || "") : "",
    picture_thumbnail: user.picture? (user.picture.thumbnail || "") : "",
    id: uuidv4(),
    favorite: false,
    course: getRandom(possibleCourses),
    bg_color: getRandom(possibleColors),
    note: getRandom(possibleNotes),
    }
    return formattedUser;
};

//обраховує вік
function calculateAge(birthdate) {
    const birthdateDate = dayjs(birthdate);
    const currentDate = dayjs();
    const age = currentDate.diff(birthdateDate, 'year');
    return age;
}

//відформатувати доданого юзера
function formatAddUser(user) {
    const formattedUser = {
    gender: user.gender || "male",
    title: user.title || "Mr",    
    full_name: user.full_name || "John Weak",
    city: user.city || "Lubny",
    state: user.state || "Poltava",
    country: user.country || "Ukraine",
    postcode: user.postcode || 35007,
    coordinates: user.coordinates || { latitude: "-42.1817", longitude: "-152.1685" },
    timezone: user.timezone || { offset: "+9:30", description: "Adelaide, Darwin" },
    email: user.email || "vv@gmail.com",
    b_date: user.b_date || "1956-12-23T19:09:19.602Z",
    age: user.b_date? calculateAge(user.b_date) : calculateAge("1956-12-23T19:09:19.602Z"),
    phone: "+" + (user.phone? user.phone.replace(/\D/g, '') : "380702020820"),
    picture_large: user.picture_large || "",
    picture_thumbnail: user.picture_thumbnail || "",
    id: uuidv4(),
    favorite: user.favorite || false,
    course: user.course || getRandom(possibleCourses),
    bg_color: user.bg_color || getRandom(possibleColors),
    note: user.note || getRandom(possibleNotes),
    }
    return formattedUser;
};

//функція, що повертає масив відформатованних об’єктів
function createUsersArray() {
    /*
    for (let i = 0; i < users.length; i++) {
        if(!users[i].full_name) {
            users[i] = formatUser(users[i]);
        }
        if (users[i].favorite) favouriteUsers.push(users[i]);
    } 
    reset();
    return users;
    */
    users = _.map(users, (user) => {
        if (!user.full_name) {
            return formatUser(user);
        }
        return user;
    });

    favouriteUsers = _.filter(users, { favorite: true });
    reset();

    return users;
}

//провалідувати юзера
function validateUser(user) {
    const validationErrors = [];
    if (!_.isString(user.full_name) || !_.startsWith(user.full_name, _.upperFirst(user.full_name)) || !_.includes(user.full_name, ' ')) {
        validationErrors.push('Full Name');
    }
    if (!_.isString(user.gender)) {
        validationErrors.push('Gender');
    }
    if (!_.isString(user.note) || !_.startsWith(user.note, _.upperFirst(user.note))) {
        validationErrors.push('Note');
    }
    if (!_.isString(user.state) || !_.startsWith(user.state, _.upperFirst(user.state))) {
        validationErrors.push('State');
    }
    if (!_.isString(user.city) || !_.startsWith(user.city, _.upperFirst(user.city))) {
        validationErrors.push('City');
    }
    if (!_.isString(user.country) || !_.startsWith(user.country, _.upperFirst(user.country))) {
        validationErrors.push('Country');
    }
    if (!_.isNumber(user.age) || user.age <= 0 || user.age >= 100) {
        validationErrors.push('Age');
    }
    if (!_.isString(user.phone)) {
        validationErrors.push('Phone');
    }
    if (!_.isString(user.email) || !_.includes(user.email, '@')) {
        validationErrors.push('Email');
    }
    /*
    if (typeof user.full_name !== "string" || user.full_name.charAt(0) !== user.full_name.charAt(0).toUpperCase() || !user.full_name.includes(" ")) {
        validationErrors.push("Full Name");
    }
    if (typeof user.gender !== "string") {
        validationErrors.push("Gender");
    }
    if (typeof user.note !== "string" || user.note.charAt(0) !== user.note.charAt(0).toUpperCase()) {
        validationErrors.push("Note");
    }
    if (typeof user.state !== "string" || user.state.charAt(0) !== user.state.charAt(0).toUpperCase()) {
        validationErrors.push("State");
    }
    if (typeof user.city !== "string" || user.city.charAt(0) !== user.city.charAt(0).toUpperCase()) {
        validationErrors.push("City");
    }
    if (typeof user.country !== "string" || user.country.charAt(0) !== user.country.charAt(0).toUpperCase()) {
        validationErrors.push("Country");
    }
    if (typeof user.age !== "number" || user.age <= 0 || user.age >= 100) {
        validationErrors.push("Age");
    }
    if (typeof user.phone !== "string" || !(user.country === "Ukraine" && /^\+380\d{9}$/.test(user.phone) || /^\+[0-9]+$/.test(user.phone))) {
        validationErrors.push("Phone");
    }
    if (typeof user.email !== "string" || !user.email.includes('@')) {
        validationErrors.push("Email");
    }
    */

    return validationErrors;
}

//фільтрація массиву обєктів за параметрами
function filterUsers(age, country, gender, photo, favorite) {
    /*
    filteredUsers = [];
    for (let user of foundUsers) {
        if ((country === null || user.country === country) && (age === null || parseInt(user.age) === parseInt(age)) 
          && (gender === null || user.gender === gender) && (photo === null || user.picture_thumbnail !== "") && (favorite === null || user.favorite === favorite)){
            filteredUsers.push(user);
        }
    }    
    sortedUsers = [...filteredUsers];
    return filteredUsers;
    */
    const filteredUsers = _.filter(foundUsers, (user) => {
        return (_.isNull(country) || user.country === country) &&
               (_.isNull(age) || _.isEqual(parseInt(user.age), parseInt(age))) &&
               (_.isNull(gender) || user.gender === gender) &&
               (_.isNull(photo) || user.picture_thumbnail !== '') &&
               (_.isNull(favorite) || user.favorite === favorite);
    });

    sortedUsers = [...filteredUsers];
    return filteredUsers;
}

//функція сортування массиву обєктів за параметрами
function sortUsers(parametr, descendingOrder) {
    /*
    if (parametr === "full_name") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return a.full_name.localeCompare(b.full_name);
            } else {
                return b.full_name.localeCompare(a.full_name);
            }
        });
    }
    else if (parametr === "course") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return a.course.localeCompare(b.course);
            } else {
                return b.course.localeCompare(a.course);
            }
        });
    }
    else if (parametr === "gender") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return a.gender.localeCompare(b.gender);
            } else {
                return b.gender.localeCompare(a.gender);
            }
        });
    }
    else if (parametr === "age") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return a.age - b.age;
            } else {
                return b.age - a.age;
            }
        });
    }
    else if (parametr === "b_day") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return new Date(a.b_date) - new Date(b.b_date);
            } else {
                return new Date(b.b_date) - new Date(a.b_date);
            }
        });
    }
    else if (parametr === "country") {
        sortedUsers.sort((a, b) => {
            if (descendingOrder) {
                return a.country.localeCompare(b.country);
            } else {
                return b.country.localeCompare(a.country);
            }
        });
    }
    return sortedUsers;
    */
    const sorted = _.sortBy(sortedUsers, parametr);
    if (descendingOrder) return sorted;
    return _.reverse(sorted);
}

//знайти в массиві об’єкт, який відповідає параметру пошуку
function findUsers(parametr) {
    clearFilters();
    if (parametr === "") {
        reset();
        return users;
    }
    if (!isNaN(parametr)) {
        parametr = parseInt(parametr);
    }
    else {
        parametr = parametr.toLowerCase();
    }
    /*
    foundUsers = [];
    for (let user of users) {
        if (typeof parametr === "string") {
            if (user.full_name.toLowerCase().includes(parametr) || (user.note && user.note.toLowerCase().includes(parametr))) {
                foundUsers.push(user); 
            }
        }
        else if (typeof parametr === "number") {
            if (user.age === parametr || (user.note && user.note.includes(parametr.toString()))) {
                foundUsers.push(user);
            }
        }
    }
    */
    foundUsers = _.filter(users, (user, parameter) => {
        if (typeof parameter === 'string') {
            return (
                user.full_name.toLowerCase().includes(parameter) ||
                (user.note && user.note.toLowerCase().includes(parameter))
            );
        } else if (typeof parameter === 'number') {
            return user.age === parameter || (user.note && user.note.includes(parameter.toString()));
        }
    });
    sortedUsers = [...foundUsers];
    return foundUsers;
}

//функція, яка повертає відсоток від загального числа обєктів в массиві, що відповідають пошуку
function persentUsers() {
    return (sortedUsers.length/users.length)*100;
}

//створити довгу карточку викладача
function createLongCardContent(user) {
    const starSrc = user.favorite ? "images/star3.png" : "images/star2.png";
    const cardContent = `
        <div class="card-name-area">
            <label class="long-card-name">Teacher Info</label>
            <button class="card-close-button">×</button>
        </div>
        <div class="card-content-area">
            <div class="main-info">
                <img class="card-photo" src=${user.picture_large} alt="teacher's photo">
                <img class="card-star" src="${starSrc}">
                <div class="imp-info">
                    <label class="card-name card-label">${user.full_name}</label>
                    <label class="card-speciality card-label">${user.course}</label>
                    <label class="card-address card-label">${user.city}, ${user.country}</label>
                    <label class="card-pi card-label">${user.age}, ${user.gender}</label>
                    <p class="card-untilBd">${calculateDaysToBD(user.b_date)}</p>
                    <a href="${user.email}" class="card-email card-label">${user.email}</a>
                    <label class="card-tel card-label">${user.phone}</label>
                </div>
            </div>
            <div class="extra-info">
                <p class="card-notes card-label">${user.note}</p>
                <div class="card-map-container">
                    <div id="map"></div>
                </div>            
             </div>
        </div>
    `;
    return cardContent;
}

//обраховує дні до наступного дня народження
function calculateDaysToBD(bdate) {
    const bday = dayjs(bdate);
    const today = dayjs();
    let nextBirthday = dayjs().year(today.year()).month(bday.month()).date(bday.date());
    if (nextBirthday.isBefore(today)) {
        nextBirthday = nextBirthday.add(1,'year');       
    }
    let daysRemaining = nextBirthday.diff(today, 'day');
    if(daysRemaining === 0) return "Celebrates birthday today!&#128570&#x1F495";
    return (daysRemaining + " days until birthday...");
}

//функція додавання/видалення з вподобаних
function starClicked(user) {
    user.favorite = !user.favorite;
    const teacherStar = document.querySelector(`#t-c-${user.id} .teacher-star`);
    teacherStar.classList.toggle("false", !user.favorite);
    if(user.favorite) {
        favouriteUsers.push(user);
        createFavouritesLine();
        createFilteredUsers();

    }
    else {
        favouriteUsers = favouriteUsers.filter(favUser => favUser.id !== user.id);
        const toDeleteFromLine = document.querySelector("#t-c-s-"+user.id);
        if (toDeleteFromLine) {
        toDeleteFromLine.remove();
        createFilteredUsers();
        }
    }
    updateUser(host, user.id, user.favorite)
}

//оновити збереженого юзера
function updateUser(url, userId, isFavorite) {
    fetch(url + `/${userId}`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(user => {
        user.favorite = isFavorite;
        return fetch(url + `/${userId}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(user),
        });
      })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to update user data');
        }
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }

//функція створення попапу карточки викладача
function showPopup(teachersCard) {
    const cardPopup = document.querySelector(".teacher-long-card-space");
    const card = document.querySelector(".teacher-long-card");
    let id;
    if (teachersCard.classList.contains("teachers-card-short")) {
        id = teachersCard.id.slice(6);
    } else {
        id = teachersCard.id.slice(4);
    }
    const user = users.find(user => user.id === id);
    card.innerHTML = createLongCardContent(user);
    card.style.backgroundColor = user.bg_color;

    var map = L.map('map').setView([parseFloat(user.coordinates.latitude), parseFloat(user.coordinates.longitude)], 5);

    L.tileLayer('https://tiles.stadiamaps.com/tiles/outdoors/{z}/{x}/{y}{r}.{ext}', {
	minZoom: 0,
	maxZoom: 15,
	attribution: '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	ext: 'png'
    }).addTo(map);

    var customIcon = L.icon({
        iconUrl: 'images/marker.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32]
    });
    L.marker([parseFloat(user.coordinates.latitude), parseFloat(user.coordinates.longitude)], { icon: customIcon }).addTo(map);
    
    cardPopup.style.display = "block";

    const cardStar = card.querySelector(".card-star");
    cardStar.addEventListener("click", function() {
        starClicked(user);
        cardStar.setAttribute("src", user.favorite ? "images/star3.png" : "images/star2.png");
    });

    const cardClosePopupButton = document.querySelector(".card-close-button");
    cardClosePopupButton.addEventListener("click", function() {
        cardPopup.style.display = "none";
    });
}

//функція створення картки викладача
function createTeachersCard(user) {
    let teachersCard = document.createElement('div');
    teachersCard.classList.add('teachers-card');
    teachersCard.setAttribute('id', 't-c-'+user.id);
    let htmlStructure = `<div class="img-wrap">`;
    user.picture_thumbnail === "" ? htmlStructure += `</img> <span class="teacher-photo photo-span">${user.full_name.split(" ")[0].charAt(0)}.${user.full_name.split(" ")[1].charAt(0)}</span>` : htmlStructure += `<img src=${user.picture_thumbnail} class="teacher-photo" alt="teacher's photo">`;
    htmlStructure += 
    `</div>
    <div class="star-container">
        <img src="images/star.png" alt="star" class="teacher-star ${user.favorite.toString()}">
    </div>
    <label class="teacher-name">${user.full_name.split(" ")[0]}</label>
    <label class="teacher-surname">${user.full_name.split(" ").slice(1).join(" ")}</label>
    <label class="teacher-speciality">${user.course}</label>
    <label class="teacher-region">${user.country}</label>`;
    teachersCard.innerHTML = htmlStructure;
    return teachersCard;
}

//функція, що заповнює грід карточками
function createTeachersGrid(users) {
    let grid = document.createElement('div');
    for (let user of users) {
        grid.appendChild(createTeachersCard(user));
    }
    const teachersList = document.querySelector('.teachers-page-list');
    teachersList.innerHTML = grid.innerHTML;

    const cardPopupButtons = document.querySelectorAll(".teacher-photo");

    cardPopupButtons.forEach(function(button) {
        button.addEventListener("click", function() {
            showPopup(button.closest(".teachers-card"));
        });
    });

}

//створити діаграму знайдених
function createPieChart() {

    const canvas = document.querySelector(".statistics-piechart");
    if (canvas.chart) {
        canvas.chart.destroy();
    }

    const foundPercentage = persentUsers();
    const allPercentage = 100 - foundPercentage;

    const foundLabels = ["Found", "All"];
    const foundData = [foundPercentage, allPercentage];

    const newChart = new Chart(canvas, {
        type: 'pie',
        data: {
            labels: foundLabels,
            datasets: [{
                data: foundData,
                backgroundColor: [
                    'rgb(255, 180, 0)',
                    'rgb(250, 250, 250)',
                ],
            }]
        },
    });
    canvas.chart = newChart;
}

//створити діаграму за віком
function createAgePieChart() {
    
    const canvas = document.querySelector(".statistics-piechart");
    if (canvas.chart) {
        canvas.chart.destroy();
    }

    const table = document.querySelector(".statistics-table");
    const rows = table.getElementsByTagName("tr");

    const ageRangeCounts = {
        "0-20": 0,
        "20-40": 0,
        "40-60": 0,
        "60-80": 0,
        "80-100": 0
    };

    for (let i = 1; i < rows.length; i++) {
        const cells = rows[i].getElementsByTagName("td");
        const age = cells[2].textContent;

        let ageRange = "";
        if (age >= 0 && age <= 20) {
            ageRange = "0-20";
        } else if (age > 20 && age <= 40) {
            ageRange = "20-40";
        } else if (age > 40 && age <= 60) {
            ageRange = "40-60";
        } else if (age > 60 && age <= 80) {
            ageRange = "60-80";
        } else {
            ageRange = "80-100";
        }
        ageRangeCounts[ageRange]++;
    }

    const ageRangeLabels = Object.keys(ageRangeCounts);
    const ageRangeData = ageRangeLabels.map(age => ageRangeCounts[age]);

    const newChart = new Chart(canvas, {
        type: 'pie',
        data: {
            labels: ageRangeLabels,
            datasets: [{
                data: ageRangeData,
                backgroundColor: [
                    'rgb(255, 180, 0)',
                    'rgb(255, 240, 0)',
                    'rgb(180, 255, 0)',
                    'rgb(0, 180, 255)',
                    'rgb(200, 0, 255)',
                ],
            }]
        }
    });
    canvas.chart = newChart;
}

//створити діаграму за гендером
function createGenderPieChart() {

    const canvas = document.querySelector(".statistics-piechart");
    if (canvas.chart) {
        canvas.chart.destroy();
    }

    const table = document.querySelector(".statistics-table");
    const rows = table.getElementsByTagName("tr");

    const genderCounts = {
        "Male": 0,
        "Female": 0
    };

    for (let i = 1; i < rows.length; i++) {
        const cells = rows[i].getElementsByTagName("td");
        const gender = cells[3].textContent;

        if (gender.toLowerCase() === "male") {
            genderCounts["Male"]++;
        } else {
            genderCounts["Female"]++;
        }
        
    }

    const genderLabels = Object.keys(genderCounts);
    const genderData = genderLabels.map(gender => genderCounts[gender]);

    const newChart = new Chart(canvas, {
        type: 'pie',
        data: {
            labels: genderLabels,
            datasets: [{
                data: genderData,
                backgroundColor: [
                    'rgb(255, 180, 0)',
                    'rgb(180, 255, 0)',
                ],
            }]
        }
    });
    canvas.chart = newChart;
}

//створити діаграму за спеціальністю
function createSpecialityPieChart() {
    
    const canvas = document.querySelector(".statistics-piechart");
    if (canvas.chart) {
        canvas.chart.destroy();
    }

    const table = document.querySelector(".statistics-table");
    const rows = table.getElementsByTagName("tr");

    const specialities = {};

    for (let i = 1; i < rows.length; i++) {
        const cells = rows[i].getElementsByTagName("td");
        const speciality = cells[1].textContent;
        
        specialities[speciality] = (specialities[speciality] || 0) + 1;
    }

    const specialitiesLabels = Object.keys(specialities);
    const specialitiesData = specialitiesLabels.map(speciality => specialities[speciality]);

    const newChart = new Chart(canvas, {
        type: 'pie',
        data: {
            labels: specialitiesLabels,
            datasets: [{
                data: specialitiesData,
                backgroundColor: [
                    'rgb(255, 50, 0)',
                    'rgb(255, 130, 0)',
                    'rgb(255, 180, 0)',
                    'rgb(255, 210, 0)',
                    'rgb(255, 240, 0)',
                    'rgb(190, 255, 0)',
                    'rgb(150, 255, 0)',
                    'rgb(100, 255, 0)',
                    'rgb(0, 255, 180)',
                    'rgb(0, 255, 230)',
                    'rgb(0, 245, 255)',
                    'rgb(0, 200, 255)',
                    'rgb(0, 160, 255)',
                    'rgb(0, 120, 255)',
                    'rgb(0, 70, 255)',
                    'rgb(50, 0, 255)',
                    'rgb(80, 0, 255)',
                    'rgb(120, 0, 255)',
                    'rgb(160, 0, 255)',
                    'rgb(200, 0, 255)',
                    'rgb(240, 0, 255)',
                    'rgb(280, 0, 255)',
                    'rgb(255, 0, 100)',
                ],
            }]
        }
    });
    canvas.chart = newChart;
}

//створити діаграму за країнами
function createCountryPieChart() {

    const canvas = document.querySelector(".statistics-piechart");
    if (canvas.chart) {
        canvas.chart.destroy();
    }

    const table = document.querySelector(".statistics-table");
    const rows = table.getElementsByTagName("tr");

    const countries = {
    };

    for (let i = 1; i < rows.length; i++) {
        const cells = rows[i].getElementsByTagName("td");
        const country = cells[4].textContent;
        
        countries[country] = (countries[country] || 0) + 1;
    }

    const countriesLabels = Object.keys(countries);
    const countriesData = countriesLabels.map(country => countries[country]);

    const newChart = new Chart(canvas, {
        type: 'pie',
        data: {
            labels: countriesLabels,
            datasets: [{
                data: countriesData,
                backgroundColor: [
                    'rgb(255, 50, 0)',
                    'rgb(255, 130, 0)',
                    'rgb(255, 180, 0)',
                    'rgb(255, 210, 0)',
                    'rgb(255, 240, 0)',
                    'rgb(190, 255, 0)',
                    'rgb(150, 255, 0)',
                    'rgb(100, 255, 0)',
                    'rgb(0, 255, 180)',
                    'rgb(0, 255, 230)',
                    'rgb(0, 245, 255)',
                    'rgb(0, 200, 255)',
                    'rgb(0, 160, 255)',
                    'rgb(0, 120, 255)',
                    'rgb(0, 70, 255)',
                    'rgb(50, 0, 255)',
                    'rgb(80, 0, 255)',
                    'rgb(120, 0, 255)',
                    'rgb(160, 0, 255)',
                    'rgb(200, 0, 255)',
                    'rgb(240, 0, 255)',
                    'rgb(280, 0, 255)',
                    'rgb(255, 0, 100)',
                ],
            }]
        }
    });
    canvas.chart = newChart;
}


//функція, що створює таблицю зі статистикою
function createStatisticTable(users) {
    /*
    let table = document.createElement('table');
    for (let user of users) {
        let tr = document.createElement('tr');

        tr.innerHTML = 
        `<td>${user.full_name}</td>
        <td>${user.course}</td>
        <td>${user.age}</td>
        <td>${user.gender}</td>
        <td>${user.country}</td>`;

        table.appendChild(tr);
    }
    */
    const statisticsTableBody = document.querySelector('.statistics-table tbody');
    statisticsTableBody.innerHTML = '';
    const rows = _.map(users, (user) => {
        return `
            <tr>
                <td>${user.full_name}</td>
                <td>${user.course}</td>
                <td>${user.age}</td>
                <td>${user.gender}</td>
                <td>${user.country}</td>
            </tr>
        `;
    });
    statisticsTableBody.innerHTML = rows.join('');
/*
    statisticsTableBody.innerHTML = table.innerHTML;
*/
    pages = Math.ceil(users.length / 10);
    createTableButtons(pages);
    const statisticsTable = document.querySelector('.statistics-table');
    statisticsTableBody.style.display = 'none';
    const buttonsSpace = document.querySelector('.table-buttons-space');
    buttonsSpace.style.display = 'none';
    const nameSort = document.querySelector('.sort-name');
    nameSort.innerHTML = 'Found';
    createPieChart();
}

//функція, що створює перемикачі сторінок
function createTableButtons(pages) {
    const pagesButtonsSpace = document.querySelector('.table-pages-button-space');
    pagesButtonsSpace.innerHTML = '';
    for (let i = 1; i <= pages; i++) {
        let button = document.createElement('button');
        button.classList.add('table-button');
        button.classList.add('table-page');
        button.setAttribute('id', 'button-' + i);
        button.innerHTML = i;
        button.addEventListener("click", () => showTablePage(parseInt(button.getAttribute('id').slice(7)), pages));
        pagesButtonsSpace.appendChild(button);
    }
    let button1 = document.getElementById('button-1');
    if(button1)
        button1.click();
    else
        showTablePage(0,0);
}

//функція, що змінює сторінку таблиці
function showTablePage(page, pages) {
    const trElements = document.querySelectorAll('tbody tr');
    trElements.forEach((tr, index) => tr.style.display = (index >= (page-1)*10 && index < (page)*10) ? 'table-row' : 'none');
    const first = document.querySelector('.table-pages-first-space');
    const last = document.querySelector('.table-pages-last-space');
    let startPage = Math.max(1, page - 1);
    let endPage = Math.min(pages, startPage + 2);
    if(endPage == pages && endPage > 2) startPage = endPage - 2;
    if(startPage == 1) {
        first.style.display = "none";
    }
    else {
        first.style.display = "block";
    }
    if(endPage == pages) {
        last.style.display = "none";
    }
    else {
        last.style.display = "block";
    }
    const children = document.querySelector('.table-pages-button-space').children;
    for (let i = 0; i < children.length; i++) {
        if (i < startPage-1 || i > endPage-1) {
            children[i].style.display = "none";
        } else {
            children[i].style.display = "block";
            if(children[i].getAttribute('id').slice(7) == page) {
                children[i].disabled = true;
            }
            else {
                children[i].disabled = false;
            }
        }
    }
}

//функція створення короткої карточки викладача
function createShortTeachersCard(user) {
    let teachersCard = document.createElement('div');
        teachersCard.classList.add('teachers-card', 'teachers-card-short');
        teachersCard.setAttribute('id', 't-c-s-'+user.id);
        let htmlStructure = `<div class="img-wrap">`;
        user.picture_thumbnail === "" ? htmlStructure += `</img> <span class="teacher-photo photo-span">${user.full_name.split(" ")[0].charAt(0)}.${user.full_name.split(" ")[1].charAt(0)}</span>` : htmlStructure += `<img src=${user.picture_thumbnail} class="teacher-photo" alt="teacher's photo">`;
        htmlStructure += 
        `</div>
        <label class="teacher-name">${user.full_name.split(" ")[0]}</label>
        <label class="teacher-surname">${user.full_name.split(" ")[1]}</label>
        <label class="teacher-region">${user.country}</label>`;
        teachersCard.innerHTML = htmlStructure;
    return teachersCard;
}

//функція, що заповнює стрічку вподобаних викладачів
function createFavouritesLine() {
    let line = document.createElement('div');
    for (let user of favouriteUsers) {
        line.appendChild(createShortTeachersCard(user));
    }
    const favoritesList = document.querySelector('.favorites-list');
    favoritesList.innerHTML = line.innerHTML;
    const cardPopupButtons = document.querySelectorAll(".teacher-photo");

    cardPopupButtons.forEach(function(button) {
        button.addEventListener("click", function() {
            showPopup(button.closest(".teachers-card"));
        });
    });
}

//функція, що встановлює опції фільтрів
function setInputs() {
    const inputAge = document.querySelector(".category-age-input");
    let users = sortUsers("age", true);
    /*
    let ages = [];
    for (let user of users) {
        if (!ages.includes(user.age)) ages.push(user.age);
    }
    */
    let ages = _.uniq(users.map(user => user.age));

    let htmlStructure = `<option class="${ages[0]}-${ages[ages.length-1]}-option option-label">${ages[0]}-${ages[ages.length-1]}</option>`;
    for (let age of ages) {
        htmlStructure += `<option class="${age}-option option-label">${age}</option>`;
    }
    inputAge.innerHTML = htmlStructure;
    const inputRegion = document.querySelector(".category-region-input");
    users = sortUsers("country", true);
    /*
    let regions = [];
    for (let user of users) {
        if (!regions.includes(user.country)) regions.push(user.country);
    }
    */
    let regions = _.uniq(users.map(user => user.country));

    htmlStructure = `<option class="all-option option-label">All</option>`;
    for (let region of regions) {
        htmlStructure += `<option class="${region}-option option-label">${region}</option>`;
    }
    inputRegion.innerHTML = htmlStructure;
}

//функція очищення фільтрів
function clearFilters() {
    document.querySelector('.category-age-input').selectedIndex = 0;
    document.querySelector('.category-region-input').selectedIndex = 0;
    document.querySelector('.category-sex-input').selectedIndex = 0;
    document.querySelector('.category-photo-input').checked = false;
    document.querySelector('.category-favorites-input').checked = false;
}

//функція, що відображає відфільтрованих користувачів 
function createFilteredUsers() {
    let age = null;
    let region = null;
    let gender = null;
    let photo = null;
    let favor = null;
    let ageInput = document.querySelector('.category-age-input').value;
    if(!ageInput.includes("-")) age = ageInput;
    let regionInput = document.querySelector('.category-region-input').value;
    if(regionInput !== "All") region = regionInput;
    let genderInput = document.querySelector('.category-sex-input').value;
    if(genderInput !== "All") gender = genderInput.toLowerCase();
    let photoInput = document.querySelector('.category-photo-input');
    if(photoInput.checked) photo = true;
    let favoritesInput = document.querySelector('.category-favorites-input');
    if(favoritesInput.checked) favor = true;        
    createTeachersGrid(filterUsers(age,region,gender,photo,favor));
    createStatisticTable(sortedUsers);

}

//функція переходу між блоками сторінки
function moveToPage(button) {
    let targetElement;
    if (button.classList.contains("teachers-page-button")) {
        targetElement = document.querySelector('.teachers-page');
    }
    else if (button.classList.contains("statistics-page-button")) {
        targetElement = document.querySelector('.statistics-page');
    }
    else if (button.classList.contains("favorites-page-button")) {
        targetElement = document.querySelector('.favorites-page');
    }
    else {
        targetElement = document.querySelector('.about-page');
    }

    if (targetElement) {
        targetElement.scrollIntoView({
            behavior: 'smooth'
        });
    }
}

//функція додавання нового юзера
function addNewUser() {
    let gender = null;
    let title = null;
    let fullName = null;
    let city = null;
    let country = null;
    let email = null;
    let phone = null;
    let date = null;
    let color = null;
    let notes = null;
    let speciality = null;

    let genderInput = document.querySelector('input[name="sex"]:checked');
    if(genderInput && genderInput.classList.contains("form-male")) gender = "male";
    else if (genderInput) gender = "female";
    if(gender && gender === "male") title = "Mr";
    else if (gender) title = "Mrs";
    fullName = document.querySelector('.form-name').value;
    city = document.querySelector('.form-city').value;
    country = document.querySelector('.form-country').value;
    email = document.querySelector('.form-email').value;
    phone = document.querySelector('.form-phone').value;
    date = document.querySelector('.form-date').value;
    color = document.querySelector('.form-color').value;
    notes = document.querySelector('.form-notes').value;
    speciality = document.querySelector('.form-speciality').value;
    let emptyFields = [];
    
    if (fullName === "") {
        emptyFields.push("Full Name");
    }
    if (city === "") {
        emptyFields.push("City");
    }
    if (genderInput === null) {
        emptyFields.push("Gender");
    }
    if (email === "") {
        emptyFields.push("Email");
    }
    if (phone === "") {
        emptyFields.push("Phone");
    }
    if (date === "") {
        emptyFields.push("Date of Birth");
    }

    if (emptyFields.length > 0) {
        alert("Please fill in the following fields:\n" + emptyFields.join("\n"));
        return;
    }
    let user = {
        gender: gender,
        title: title,    
        full_name: fullName,
        city: city,
        country: country,
        email: email,
        b_date: date,
        phone: phone,
        favorite: false,
        course: speciality,
        bg_color: color,
        note: notes
    }
    user = formatAddUser(user);
    const validationErrors = validateUser(user);
    if (validationErrors.length > 0) {
        alert("The following fields are not correctly filled:\n" + validationErrors.join("\n"));
        return;
    }
    else {
        
        let contains = false;
        for (let u of users) {
            if (u.full_name === user.full_name) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            users.push(user);
            reset();
            createTeachersGrid(users);
            createStatisticTable(users);
            createFavouritesLine();
            formPopup.style.display = 'none';
            formPopup.removeChild;
            clearFilters();
            httpPost(host, user)
            .then(() => {
            })
            .catch((error) => console.error(error));
        }
        else {
            alert("User already exists");
        }
    }
}

function httpPost(url, data) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.onload = function () {
        if (this.status === 201) {
          resolve(this.response);
        } else {
          const error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      };
      xhr.onerror = function () {
        reject(new Error('Network Error'));
      };
      xhr.send(JSON.stringify(data));
    });
 }
  


document.querySelector(".next-button").addEventListener("click", function () {
    const favoritesList = document.querySelector(".favorites-list");
    const firstCard = favoritesList.firstElementChild;
    favoritesList.removeChild(firstCard);
    favoritesList.appendChild(firstCard);
});

document.querySelector(".previous-button").addEventListener("click", function () {
    const favoritesList = document.querySelector(".favorites-list");
    const lastCard = favoritesList.lastElementChild;
    favoritesList.removeChild(lastCard);
    favoritesList.insertBefore(lastCard, favoritesList.firstElementChild);
});


const searchButton = document.querySelector(".search-button");
const searchInput = document.querySelector(".search-input")
searchButton.addEventListener('click', function() {
    const inputValue = searchInput.value.trim();
    createTeachersGrid(findUsers(inputValue));
    createStatisticTable(sortedUsers);
})

const categoryInput = document.querySelectorAll('.category-input');
categoryInput.forEach(function(input) {
    input.addEventListener('change', createFilteredUsers);
})

const pageButton = document.querySelectorAll('.page-button');
pageButton.forEach(function(button) {
    button.addEventListener("click", function() {
        moveToPage(button);
    });
});

const tableFirstButton = document.querySelector('.table-first');
tableFirstButton.addEventListener('click', function() {
    showTablePage(1,pages);
});

const tableLastButton = document.querySelector('.table-last');
tableLastButton.addEventListener('click', function() {
    showTablePage(pages,pages);
});

const sortButtons = document.querySelectorAll('th');
sortButtons.forEach(function(button) {
    button.addEventListener('click', function() {
        if(button.classList.contains("sort-name")) {
            if(button.classList.contains("des")) createStatisticTable(sortUsers("full_name", true));
            else createStatisticTable(sortUsers("full_name", false));
            createPieChart();
        }
        else if(button.classList.contains("sort-speciality")) {
            if(button.classList.contains("des")) createStatisticTable(sortUsers("course", true));
            else createStatisticTable(sortUsers("course", false));
            createSpecialityPieChart();
        }
        else if(button.classList.contains("sort-age")) {
            if(button.classList.contains("des")) createStatisticTable(sortUsers("age", true));
            else createStatisticTable(sortUsers("age", false));
            createAgePieChart();
        }
        else if(button.classList.contains("sort-gender")) {
            if(button.classList.contains("des")) createStatisticTable(sortUsers("gender", true));
            else createStatisticTable(sortUsers("gender", false));
            createGenderPieChart();
        }
        else if(button.classList.contains("sort-nationality")) {
            if(button.classList.contains("des")) createStatisticTable(sortUsers("country", true));
            else createStatisticTable(sortUsers("country", false));
            createCountryPieChart();
        }
        button.classList.toggle("des");
    });
})


const formPopupButtons = document.querySelectorAll(".add-button");
const formPopup = document.querySelector(".add-form-space");
formPopupButtons.forEach(function(button) {
    button.addEventListener("click", function() {
        let form = document.createElement('form');
        form.classList.add('add-form');
        form.innerHTML = `
               <div class="name-area">
                  <label class="add-form-name">Add Teacher</label>
                  <button class="form-close-button">×</button>
               </div>
               <div class="input-area">
                  <div class="form-name-space form-space">
                     <label class="form-name-label form-label">Name</label>
                     <input type="text" class="form-name form-input" placeholder="Enter name">
                  </div>
                  <div class="form-speciality-space form-space">
                     <label class="form-speciality-label form-label">Speciallity</label>
                     <select class="form-speciality form-input">
                        <option class="math-option option-label">Mathematics</option>
                        <option class="chem-option option-label">Chemistry</option>
                        <option class="voc-option option-label">Vocal</option>
                        <option class="comp-option option-label">Computer Science</option>
                        <option class="biol-option option-label">Biology</option>
                        <option class="med-option option-label">Medicine</option>
                        <option class="chess-option option-label">Chess</option>
                        <option class="phys-option option-label">Physics</option>
                        <option class="art-option option-label">Art</option>
                        <option class="eng-option option-label">English</option>
                        <option class="stat-option option-label">Statistics</option>
                     </select>
                  </div>
                  <div class="address">
                     <div class="form-country-space form-space">
                        <label class="form-country-label form-label">Country</label>
                        <select class="form-country form-input">
                           <option class="ua-option option-label">Ukraine</option>
                           <option class="in-option option-label">India</option>
                           <option class="den-option option-label">Denmark</option>
                           <option class="belg-option option-label">Belgium</option>
                           <option class="ch-option option-label">China</option>
                           <option class="uk-option option-label">UK</option>
                           <option class="bel-option option-label">Belarus</option>
                           <option class="pol-option option-label">Poland</option>
                           <option class="viet-option option-label">Vietnam</option>
                           <option class="usa-option option-label">USA</option>
                           <option class="irl-option option-label">Ireland</option>
                           <option class="en-option option-label">England</option>
                           <option class="aust-option option-label">Austria</option>
                           <option class="ital-option option-label">Italy</option>
                           <option class="neth-option option-label">Netherlands</option>
                           <option class="scot-option option-label">Scotland</option>
                           <option class="fr-option option-label">France</option>
                        </select>
                     </div>
                     <div class="form-city-space form-space">
                        <label class="form-city-label form-label">City</label>
                        <input type="text" class="form-city form-input">
                     </div>
                  </div>
                  <div class="contacts">
                     <div class="form-email-space form-space">
                        <label class="form-email-label form-label">Email</label>
                        <input type="email" class="form-email form-input">
                     </div>
                     <div class="form-phone-space form-space">
                        <label class="form-phone-label form-label">Phone</label>
                        <input type="tel" class="form-phone form-input">
                     </div>
                  </div>
                  <div class="form-date-space form-space">
                     <label class="form-date-label form-label">Date of birth</label>
                     <input type="date" class="form-date form-input" lang="en-gb">
                  </div>
                  <div class="form-sex-space form-space">
                     <label class="form-sex-label form-label">Sex</label>
                     <div class="radio form-gender">
                        <label class="radio-male form-label">Male</label>
                        <input type="radio" class="form-sex form-male form-input" name="sex">
                        <label class="radio-female form-label">Female</label>
                        <input type="radio" class="form-sex form-female form-input" name="sex">
                     </div>
                  </div>
                  <div class="form-color-space form-space">
                     <label class="form-color-label form-label">Background color</label>
                     <div class="color-wrap">
                        <img src="images/color.png" class="color-pict">
                        <input type="color" class="form-color form-input" value="#ffffff">
                     </div> 
                  </div>
                  <div class="form-notes-space form-space">
                     <label class="form-sex-label form-label">Notes (optional)</label>
                     <textarea class="form-notes form-input" rows="5"></textarea>            
                  </div>
                  <div class="form-button-space form-space">
                     <input type="button" class="form-button form-input form-add-button" value="Add">
                  </div>
               </div>
            `
            formPopup.appendChild(form);
            formPopup.style.display = 'block';
            const formClosePopupButton = document.querySelector(".form-close-button");
            const formAddButton = document.querySelector(".form-add-button");
            formClosePopupButton.addEventListener("click", function() {
                formPopup.removeChild(form);
                formPopup.style.display = 'none';
                clearFilters();
            });
            formAddButton.addEventListener("click", function() {
                addNewUser();
            });
    });
});


getUsers();


/*
async function clearUsersData() {
    try {
        const response = await fetch('http://localhost:3000/posts', {
            method: 'DELETE'
        });
        if (!response.ok) {
            throw new Error('Failed to clear data');
        }
        console.log('Data cleared successfully');
    } catch (error) {
        console.error('Error:', error);
    }
}
*/